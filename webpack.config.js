const path = require("path");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const webpack = require("webpack");

module.exports = (env = {}) => {
  return {
    entry: "./src/index.jsx",
    output: {
      path: path.resolve(__dirname, "public/"),
      publicPath: "/",
      filename: "bundle.js",
      chunkFilename: "[name].bundle.js"
    },
    devtool:
      env.NODE_ENV === "production"
        ? "source-map"
        : "cheap-module-eval-source-map",
    module: {
      rules: [
        { test: /\.jsx?$/, loader: "babel-loader" },
        {
          test: /\.css$/,
          use: ExtractTextPlugin.extract({
            fallback: "style-loader",
            use: [
              {
                loader: "css-loader",
                options: {
                  importLoaders: 1,
                  modules: true,
                  camelCase: true
                }
              },
              "postcss-loader"
            ]
          })
        }
      ]
    },
    plugins: [
      new ExtractTextPlugin("styles.css"),
      new webpack.DefinePlugin({
        "process.env.NODE_ENV": JSON.stringify(env.NODE_ENV),
        "process.env.API_URL": JSON.stringify(env.NODE_ENV === "production" ? "https://gubbins-api.now.sh" : "http://localhost:3000")
      })
    ].concat(
      env.NODE_ENV === "production"
        ? [new webpack.optimize.UglifyJsPlugin()]
        : []
    )
  };
}
