import "./polyfill";
import { h, render } from "preact";
import Router from "preact-router";
import Clipboard from "clipboard";

import GubbinsForm from "./GubbinsForm/index.jsx";
import Gub from "./Gub/index.jsx";

render(<Router>
  <GubbinsForm path="/" />
  <Gub path="/gub/:id/key/:key" />
  <Gub path="/gub/:id/key/:key/confirm" confirm={true} />
  <GubbinsForm default />
</Router>, document.getElementById("gubbins-root"));
