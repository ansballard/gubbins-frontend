import { h, Component } from "preact";
import Clipboard from "clipboard";
import styles from "./index.css";

export default class GubbinsForm extends Component {
  constructor() {
    super();
    this.state = {
      form: {},
      gub: undefined,
      clipboard: undefined
    };
    this.generateGub = this.generateGub.bind(this);
    this.updateForm = this.updateForm.bind(this);
  }
  componentDidMount() {
    const clipboard = new Clipboard(".clipboard");
  }
  componentWillUnmount() {
    this.state.clipboard && this.state.clipboard.destroy();
  }
  generateGub(e, { gub, charges, from, lifespan, confirm }) {
    e.preventDefault();
    return fetch(`${process.env.API_URL}/gub`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        gub,
        charges,
        from,
        lifespan,
        confirm
      })
    })
      .then(res => res.json())
      .then(({ url }) => {
        this.setState(() => ({
          gub: `${window.location.origin}${url}${confirm ? "/confirm" : ""}`
        }));
      });
  }
  updateForm(e) {
    this.setState(({ form }) => ({
      form: {
        ...form,
        [e.target.name]:
          e.target.type !== "checkbox" ? e.target.value : e.target.checked
      }
    }));
  }
  render() {
    const { form, gub, copied, transitionDuration } = this.state;
    return (
      <section class={styles.gubbinsFormWrapper}>
        <form onSubmit={e => this.generateGub(e, form)}>
          <div className={styles.gubbinsFormFieldset}>
            <label for="gub">Gub</label>
            <input
              type="text"
              name="gub"
              id="gub"
              required
              onChange={this.updateForm}
            />
          </div>
          <div className={styles.gubbinsFormFieldset}>
            <label for="charges">Charges</label>
            <input
              min="1"
              type="number"
              name="charges"
              id="charges"
              placeholder="1"
              onChange={this.updateForm}
            />
          </div>
          <div className={styles.gubbinsFormFieldset}>
            <label for="lifespan">Lifespan</label>
            <input
              min="0"
              step="1"
              type="number"
              name="lifespan"
              id="lifespan"
              placeholder="24 Hours"
              onChange={this.updateForm}
            />
          </div>
          <div className={styles.gubbinsFormFieldset}>
            <label for="from">From</label>
            <input
              type="text"
              name="from"
              id="from"
              onChange={this.updateForm}
            />
          </div>
          <div className={styles.gubbinsFormFieldset}>
            <label for="confirm">Confirm</label>
            <input
              type="checkbox"
              name="confirm"
              id="confirm"
              onChange={this.updateForm}
            />
          </div>
          <div
            className={[
              styles.gubbinsFormFieldset,
              styles.gubbinsFormSubmitWrapper
            ].join(" ")}
          >
            <button type="submit">Generate</button>
          </div>
          {gub && (
            <div className={styles.gubUrlWrapper}>
              <input readonly type="text" id="gub" value={gub} />
              <button type="button" class="clipboard" data-clipboard-text={gub}>
                Copy
              </button>
            </div>
          )}
        </form>
      </section>
    );
  }
}
