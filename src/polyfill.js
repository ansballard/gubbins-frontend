// __webpack_public_path__ = process.env.ASSET_PATH;

if(process.env.NODE_ENV !== "production") {
  require("preact/devtools");
}
if(typeof Promise === "undefined") {
  require("promiscuous/promiscuous-browser-full");
}
if(typeof fetch === "undefined") {
  require("unfetch/polyfill");
}
