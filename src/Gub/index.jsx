import { h, Component } from "preact";
import Clipboard from "clipboard";
import styles from "./index.css";

export default class Gub extends Component {
  constructor() {
    super();
    this.state = {
      gub: undefined
    };
    this.getGub = this.getGub.bind(this);
  }
  componentDidMount() {
    !this.props.confirm && this.getGub(this.props.matches);
  }
  getGub({ id, key }) {
    fetch(`${process.env.API_URL}/gub/${id}/key/${key}`, {
      method: "GET"
    })
      .then(
        res =>
          res.status !== 200
            ? Promise.reject(
                res.status === 404 ? "No Gub Found" : "Server Error"
              )
            : res
      )
      .then(res => res.json())
      .then(gub => {
        this.setState(() => ({
          gub
        }));
      })
      .catch(gub => {
        this.setState(() => ({
          gub,
          error: true
        }));
      });
  }
  render() {
    return (
      <div>
        {!this.state.gub && (
          <button onClick={e => this.getGub(this.props.matches)}>
            Confirm
          </button>
        )}
        {this.state.gub && (
          <code style={{color: (this.state.error ? "red" : undefined)}}>
            <pre>{JSON.stringify(this.state.gub, undefined, 2)}</pre>
          </code>
        )}
      </div>
    );
  }
}
